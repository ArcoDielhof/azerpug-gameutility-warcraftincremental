local GlobalAddonName, AGU = ...

local addonMain = {}  
local money = 0
local globalModifier = 1
local clickModifier = 1

function addonMain:main()
    WarcraftIncrementalMainFrame = CreateFrame("FRAME", "WarcraftIncrementalMainFrame", UIParent, "BackdropTemplate")
    WarcraftIncrementalMainFrame:SetSize(400, 300)
    WarcraftIncrementalMainFrame:SetPoint("CENTER")
    WarcraftIncrementalMainFrame:SetBackdrop( { 
        bgFile = "bgFile", 
        edgeFile = "edgeFile", tile = false, tileSize = 0, edgeSize = 32, 
        insets = { left = 0, right = 0, top = 0, bottom = 0 }
      });

    WarcraftIncrementalMainFrame.moneyText = WarcraftIncrementalMainFrame:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
    WarcraftIncrementalMainFrame.moneyText:SetWidth(150)
    WarcraftIncrementalMainFrame.moneyText:SetHeight(20)
    WarcraftIncrementalMainFrame.moneyText:SetPoint("TOPLEFT", 5, -5)

    WarcraftIncrementalMainFrame.clickModifierText = WarcraftIncrementalMainFrame:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
    WarcraftIncrementalMainFrame.clickModifierText:SetWidth(150)
    WarcraftIncrementalMainFrame.clickModifierText:SetHeight(20)
    WarcraftIncrementalMainFrame.clickModifierText:SetPoint("TOPLEFT", 5, -25)

    WarcraftIncrementalMainFrame.incrementButton = CreateFrame("BUTTON", nil, WarcraftIncrementalMainFrame, "UIPanelButtonTemplate")
    WarcraftIncrementalMainFrame.incrementButton:SetText("Increment Money")
    WarcraftIncrementalMainFrame.incrementButton:SetPoint("BOTTOM", 0, 10)
    WarcraftIncrementalMainFrame.incrementButton:SetSize(100, 20)
    WarcraftIncrementalMainFrame.incrementButton:SetScript("OnClick", function()
        money = money + (1 * clickModifier)
        addonMain:updateText()
    end)

    WarcraftIncrementalMainFrame.upgradeClickModifier = CreateFrame("BUTTON", nil, WarcraftIncrementalMainFrame, "UIPanelButtonTemplate")
    WarcraftIncrementalMainFrame.upgradeClickModifier:SetText("Increment Modifier")
    WarcraftIncrementalMainFrame.upgradeClickModifier:SetPoint("BOTTOM", 110, 10)
    WarcraftIncrementalMainFrame.upgradeClickModifier:SetSize(100, 20)
    WarcraftIncrementalMainFrame.upgradeClickModifier:SetScript("OnClick", function()
        clickModifier = clickModifier + 1
        addonMain:updateText()
    end)

    addonMain:updateText()
end

function addonMain:updateText()
    WarcraftIncrementalMainFrame.moneyText:SetText(string.format("Money: %d", money))
    WarcraftIncrementalMainFrame.clickModifierText:SetText(string.format("Modifier: %d", clickModifier))
end

addonMain:main()